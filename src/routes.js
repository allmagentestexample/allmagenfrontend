import React from "react";

import { Icon } from "@chakra-ui/react";
import {
  MdHome,
  MdAreaChart,
  MdUploadFile,
  MdTableView
} from "react-icons/md";

// Admin Imports
import MainPage from "views/admin/home";
import DiagramPage from "views/admin/diagram"
import Import from "views/admin/import";
import DataTables from "views/admin/dataTables";


const routes = [
  {
    name: "Home",
    layout: "/admin",
    path: "/main",
    icon: <Icon as={MdHome} width='20px' height='20px' color='inherit' />,
    component: MainPage,
  },
  {
    name: "Diagrams",
    layout: "/admin",
    path: "/diagram",
    icon: <Icon as={MdAreaChart} width='20px' height='20px' color='inherit' />,
    component: DiagramPage,
  },
  {
    name: "Tables",
    layout: "/admin",
    icon: <Icon as={MdTableView} width='20px' height='20px' color='inherit' />,
    path: "/data-tables",
    component: DataTables,
  },
  {
    name: "Import",
    layout: "/admin",
    path: "/profile",
    icon: <Icon as={MdUploadFile} width='20px' height='20px' color='inherit' />,
    component: Import,
  }
];

export default routes;

// Chakra imports
import {
  Box,
  Button,
  Flex,
  Icon,
  Text,
  useColorModeValue
} from "@chakra-ui/react";
// Custom components
import Card from "components/card/Card.js";
import React, { useState } from "react";
// Assets
import { MdUpload } from "react-icons/md";
import Dropzone from "./Dropzone";

export default function Upload(props) {
  const { used, total, ...rest } = props;
  // Chakra Color Mode
  const textColorPrimary = useColorModeValue("secondaryGray.900", "white");
  const brandColor = useColorModeValue("brand.500", "white");
  const textColorSecondary = "gray.400";

  const [fileData, setFileData] = useState([]);

  const uploadData = () => {
    console.log("FileData: ", fileData)


    for (var i = 0; i < fileData.length; i++) {
      var formData = new FormData();
      let file = fileData[i];
      formData.append('file', file);
      fetch("http://127.0.0.1:8448/api/import/", {
        method: 'post',
        body: formData,
      })
        .then(
          (result) => {
            console.log("OK")
          },
          (error) => {
            console.log("error");
          }
        )
    }
  }

  return (
    <Card {...rest} mb='20px' align='center' p='20px' minH='300px'>
      <Flex h='100%' direction={{ base: "column", "2xl": "row" }}>
        <Dropzone
          w={{ base: "100%", "2xl": "268px" }}
          me='36px'
          maxH={{ base: "60%", lg: "50%", "2xl": "100%" }}
          minH={{ base: "60%", lg: "50%", "2xl": "100%" }}
          fileData={{ func: { setFileData } }}

          content={
            <Box>
              <Icon as={MdUpload} w='80px' h='80px' color={brandColor} />
              <Flex justify='center' mx='auto' mb='12px'>
                <Text fontSize='xl' fontWeight='700' color={brandColor}>
                  Upload Files
                </Text>
              </Flex>
              <Text fontSize='sm' fontWeight='500' color='secondaryGray.500'>
                CSV, TXT and BIN files are allowed
              </Text>
            </Box>
          }
        />
        <Flex direction='column' pe='44px'>

          <Text
            color={textColorPrimary}
            fontWeight='bold'
            textAlign='start'
            fontSize='2xl'
            mt={{ base: "20px", "2xl": "50px" }}>
            {fileData.length === 0 ? "Select file" : "Selected files:"}
          </Text>

          {
            fileData.slice(0, 4).map((row) => (
              <Text
                key={row.name}
                color={textColorSecondary}
                fontSize='md'
                my={{ base: "auto", "2xl": "10px" }}
                textAlign='start'>
                {row.name}
              </Text>
            ))}


          <Flex w='100%'>{
            fileData.length > 0 &&

            <Button
              onClick={uploadData}
              me='100%'
              mb='50px'
              w='140px'
              minW='140px'
              mt={{ base: "20px", "2xl": "auto" }}
              variant='brand'
              fontWeight='500'>
              Upload now
            </Button>
          }
          </Flex>

        </Flex>
      </Flex>
    </Card>
  );
}

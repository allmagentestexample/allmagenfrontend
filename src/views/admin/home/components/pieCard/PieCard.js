// Chakra imports
import { Box, Flex, Text, Select, useColorModeValue } from "@chakra-ui/react";
// Custom components
import Card from "components/card/Card.js";
import PieChart from "./PieChart"
import { pieChartOptions } from "variables/charts";
import { VSeparator } from "components/separator/Separator";
import React, { useEffect, useState } from "react";
import { GetStatisticCount } from "modem/statistic";

export default function Conversion(props) {
  const { ...rest } = props;

  // Chakra Color Mode
  const textColor = useColorModeValue("secondaryGray.900", "white");

  const [count, setCounts] = useState({
    "event_count": 0,
    "impression_count": 0
  });

  useEffect(() => {

    GetStatisticCount().then(
      (result) => { setCounts(result); },
      (error) => { console.log(error); }
    )
  }, [])

  return (

    <Card p='20px' align='center' direction='column' w='100%' {...rest}>
      <Flex
        px={{ base: "0px", "2xl": "10px" }}
        justifyContent='space-between'
        alignItems='center'
        w='100%'
        mb='8px'>
        <Text color={textColor}
          fontSize='22px'
          fontWeight='700'
          lineHeight='100%'>
          Impressions and events
        </Text>
      </Flex>
      <PieChart
        h='100%'
        w='100%'
        event_count={count.event_count}
        impression_count={count.impression_count}
        chartOptions={pieChartOptions}
      />
      <Flex direction='row' py='5px'>
        <Flex direction='column' py='5px'>
          <Flex align='center'>
            <Box h='8px' w='8px' bg='brand.500' borderRadius='50%' me='4px' />
            <Text
              fontSize='xs'
              color='secondaryGray.600'
              fontWeight='700'
              mb='5px'>
              Impressions with events
            </Text>
          </Flex>
          <Text fontSize='lg' color={textColor} fontWeight='700'>
            {count.event_count}
          </Text>
        </Flex>
        <VSeparator mx={{ base: "60px", xl: "60px", "2xl": "60px" }} />
        <Flex direction='column' py='5px' me='10px'>
          <Flex align='center'>
            <Box h='8px' w='8px' bg='#6AD2FF' borderRadius='50%' me='4px' />
            <Text
              fontSize='xs'
              color='secondaryGray.600'
              fontWeight='700'
              mb='5px'>
              Impressions without events
            </Text>
          </Flex>
          <Text fontSize='lg' color={textColor} fontWeight='700'>
            {count.impression_count - count.event_count}
          </Text>
        </Flex>
      </Flex>
    </Card>
  );
}

import React from "react";
import ReactApexChart from "react-apexcharts";
import { GetStatisticCount } from "modem/statistic";


class PieChart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      event_count: 50,
      impression_count: 50,
      chartOptions: {},
    };
  }

  componentDidMount() {
    GetStatisticCount().then(
      (result) => {
        this.setState({
          event_count: result.event_count,
          impression_count: result.impression_count,
          chartOptions: this.props.chartOptions,
        });
      },
      (error) => {
        console.log(error);
      }
    )
      }

       getChartData() {
        var e1=this.state.event_count / this.state.impression_count * 100;
        var e2 =  100 - this.state.event_count / this.state.impression_count * 100;
        return[e1, e2];
      }
  render() {
    return (
      <ReactApexChart
        options={this.state.chartOptions}
        series={this.getChartData()}
        type='pie'
        width='100%'
        height='55%'
      />
    );
  }
}

export default PieChart;

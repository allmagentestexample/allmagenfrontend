
import {
  Box,
  SimpleGrid,
  useColorModeValue,
} from "@chakra-ui/react";

// Custom components
import React from "react";
import ComplexTable from "views/admin/home/components/ComplexTable";
import PieCard from "views/admin/home/components/pieCard/PieCard"
import { columnsDataComplex } from "./variables/columnsData";

export default function UserReports() {
  // Chakra Color Mode
  const brandColor = useColorModeValue("brand.500", "white");
  const boxBg = useColorModeValue("secondaryGray.300", "whiteAlpha.100");
  return (
    <Box pt={{ base: "130px", md: "80px", xl: "80px" }}>

      <SimpleGrid columns={{ base: 2, md: 2, xl: 2, sm: 1 }} gap='20px' mb='20px'>
        <PieCard />
        <ComplexTable
          columnsData={columnsDataComplex}
        />
      </SimpleGrid>
    </Box>
  );
}

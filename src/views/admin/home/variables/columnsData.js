export const columnsDataCheck = [
  {
    Header: "NAME",
    accessor: "name",
  },
  {
    Header: "PROGRESS",
    accessor: "progress",
  },
  {
    Header: "QUANTITY",
    accessor: "quantity",
  },
  {
    Header: "DATE",
    accessor: "date",
  },
];
export const columnsDataComplex = [
  {
    Header: "EVENT NAME",
    accessor: "event_title",
  },
  {
    Header: "COUNT",
    accessor: "count",
  },
  {
    Header: "PER CENT",
    accessor: "percent",
  },
];

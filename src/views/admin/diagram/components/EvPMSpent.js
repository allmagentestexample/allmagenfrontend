// Chakra imports
import {
  Box,
  Button,
  Flex,
  Icon,
  Text,
  useColorModeValue,
} from "@chakra-ui/react";
// Custom components
import Card from "components/card/Card.js";
import LineChart from "components/charts/LineChart";
import ApexChart from "./apex";
import React, { useContext, useEffect } from "react";
import { IoCheckmarkCircle } from "react-icons/io5";
import { MdBarChart, MdOutlineCalendarToday } from "react-icons/md";
// import ApexChart from "./components/apex";
import MenuDate from "./DateMenu"
import MenuEvent from "./EventMenu"
// Assets
import { RiArrowUpSFill } from "react-icons/ri";

import { DiagramPageContext } from "contexts/DiagramContext";

export default function EvpmSpent(props) {
  const { ...rest } = props;

  // Chakra Color Mode
  const value = useContext(DiagramPageContext);
  const textColor = useColorModeValue("secondaryGray.900", "white");
  const textColorSecondary = useColorModeValue("secondaryGray.600", "white");
  const boxBg = useColorModeValue("secondaryGray.300", "whiteAlpha.100");
  const iconColor = useColorModeValue("brand.500", "white");
  const bgButton = useColorModeValue("secondaryGray.300", "whiteAlpha.100");
  const bgHover = useColorModeValue(
    { bg: "secondaryGray.400" },
    { bg: "whiteAlpha.50" }
  );
  const bgFocus = useColorModeValue(
    { bg: "secondaryGray.300" },
    { bg: "whiteAlpha.100" }
  );



  return (
    <Card
      justifyContent='center'
      align='center'
      direction='column'
      w='100%'
      mb='0px'
      {...rest}>
      <Flex w='100%' flexDirection={{ base: "column", lg: "row" }}>
        <Flex flexDirection='column' w='10%' me='20px' mt='28px'>
          <MenuEvent />
        </Flex>
        <Box minH='260px' minW='90%' mt='auto'>
          <Flex align='center' w='100%' px='15px' py='10px'>
            <Text
              me='auto'
              color={textColor}
              fontSize='xl'
              fontWeight='700'
              lineHeight='100%'>
              EvPT_{value.value.interval}
            </Text>
            <MenuDate />
          </Flex>
          <Flex w='100%' flexDirection={{ base: "column", lg: "row" }}>
            <Box minH='260px' minW='100%' mt='auto'>
              <ApexChart />
            </Box>
          </Flex>
        </Box>
      </Flex>
    </Card>
  );
}

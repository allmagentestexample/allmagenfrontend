// Chakra imports
import {
  Box,
  Flex,
  Text,
  useColorModeValue,
} from "@chakra-ui/react";
// Custom components
import Card from "components/card/Card.js";
import ApexChart from "./apex";
import React, { useContext, useEffect } from "react";
// import ApexChart from "./components/apex";
import MenuDate from "./DateMenu"
// Assets

import { DiagramPageContext } from "contexts/DiagramContext";

export default function TotalSpent(props) {
  const { ...rest } = props;

  // Chakra Color Mode
  const value = useContext(DiagramPageContext);
  const textColor = useColorModeValue("secondaryGray.900", "white");


  return (
    <Card
      justifyContent='center'
      align='center'
      direction='column'
      w='100%'
      mb='0px'
      {...rest}>
      <Flex justify='space-between' ps='0px' pe='20px' pt='5px'>

      </Flex>
      <Flex align='center' w='100%' px='15px' py='10px'>
        <Text
          me='auto'
          color={textColor}
          fontSize='xl'
          fontWeight='700'
          lineHeight='100%'>
            CTR_{value.value.interval}
        </Text>
        <MenuDate />
      </Flex>
      <Flex w='100%' flexDirection={{ base: "column", lg: "row" }}>
        <Box minH='260px' minW='100%' mt='auto'>
          <ApexChart />
        </Box>
      </Flex>
    </Card>
  );
}

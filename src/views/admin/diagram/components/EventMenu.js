import React, { useContext, useEffect, useState } from "react";
import { DiagramPageContext } from "contexts/DiagramContext";
// Chakra imports
import {
  Flex,
  Text,
  SimpleGrid,
  useColorModeValue,
  Checkbox
} from "@chakra-ui/react";

export default function Banner(props) {
  const { ...rest } = props;

  const textColor = useColorModeValue("secondaryGray.500", "white");

  const value = useContext(DiagramPageContext)

  const [events, setEvents] = useState({
    registration: true,
    content: false,
    signup: false,
    lead: false,
    misc: false
  });

  const handleChange = (ev) => {
    console.log("Before eventList:" + ev.target.name, value.value.eventList)
    var event = events;
    switch (ev.target.name) {
      case "Registration": {
        event.registration = !events.registration;
        break;
      }
      case "Content": {
        event.content = !events.content;
        break;
      }
      case "Signup": {
        event.signup = !events.signup;
        break;
      }
      case "Lead": {
        console.log(event.lead)
        event.lead = !events.lead;
        console.log(event.lead)
        break;
      }
      case "Misc": {
        event.misc = !events.misc;
        break;
      }
    }
    setEvents(event);

    value.value.eventList = createEventList();
    console.log("After value:", value.value.eventList)
    value.value.update(true)
  };

  const createEventList = () => {
    var list = [];
    if (events.registration) {
      list.push("registration")
    }
    if (events.content) {
      list.push("content")
    }
    if (events.signup) {
      list.push("signup")
    }
    if (events.lead) {
      list.push("lead")
    }
    if (events.misc) {
      list.push("misc")
    }
    return list;
  }


  return (
    <SimpleGrid
      mb='20px'
      columns={{ sm: 1, md: 1 }}
      spacing={{ base: "20px", xl: "20px" }}>
      <Text
        color={textColor}
        fontSize='19px'
        textAlign='start'
        fontWeight='700'
        lineHeight='100%'>
        Events
      </Text>
      <Flex align='center'>
        <Checkbox me='16px' defaultChecked={events.registration} name="Registration" onChange={handleChange} />
        <Text
          color={textColor}
          fontSize='15px'
          fontWeight='700'
          lineHeight='100%'>
          Registration
        </Text>
      </Flex>
      <Flex align='center'>
        <Checkbox me='16px' defaultChecked={events.content} name="Content" onChange={handleChange} />
        <Text
          color={textColor}
          fontSize='15px'
          fontWeight='700'
          lineHeight='100%'>
          Content
        </Text>
      </Flex>
      <Flex align='center'>
        <Checkbox me='16px' defaultChecked={events.signup} name="Signup" onChange={handleChange} />
        <Text
          color={textColor}
          fontSize='15px'
          fontWeight='700'
          lineHeight='100%'>
          Signup
        </Text>
      </Flex>
      <Flex align='center'>
        <Checkbox me='16px' defaultChecked={events.lead} name="Lead" onChange={handleChange} />
        <Text
          color={textColor}
          fontSize='15px'
          fontWeight='700'
          lineHeight='100%'>
          Lead
        </Text>
      </Flex>
      <Flex align='center'>
        <Checkbox me='16px' defaultChecked={events.misc} name="Misc" onChange={handleChange} />
        <Text
          color={textColor}
          fontSize='15px'
          fontWeight='700'
          lineHeight='100%'>
          Misc
        </Text>
      </Flex>
    </SimpleGrid>
  );
}

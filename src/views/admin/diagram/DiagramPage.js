import { lineChartOptionsTotalSpent, lineChartDataTotalSpent } from "variables/charts";
import { SimpleGrid } from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
import { DiagramPageContext } from "contexts/DiagramContext";
import CtrSpent from "./components/CtrSpent";
import EvpmSpent from "./components/EvPMSpent";
import { Get_Ctr, Get_EvPM } from "modem/statistic";

export default function DiagramPage(props) {
    const { ...rest } = props;


    const [updateCtr, setUpdateCrt] = useState(true)
    const [updateEvPm, setUpdateEvPM] = useState(true)



    const [valueCtr, setValueCtr] = useState({
        data: lineChartDataTotalSpent,
        options: lineChartOptionsTotalSpent,
        interval: "DAILY",
        update: setUpdateCrt
    });
    const [valueEvpm, setValueEvmp] = useState({
        data: lineChartDataTotalSpent,
        options: lineChartOptionsTotalSpent,
        interval: "DAILY",
        update: setUpdateEvPM,
        eventList: ["registration"]
    });

    useEffect(() => {
        if (updateCtr == true) {
            var ev1 = { period: valueCtr.interval };
            Get_Ctr(ev1)
                .then(
                    (result) => {
                        toFixedValue(result.yaxis);
                        lineChartOptionsTotalSpent.xaxis.categories = result.xaxis;
                        var interval = valueCtr.interval;
                        setValueCtr({ data: result.yaxis, options: lineChartOptionsTotalSpent, interval: interval, update: setUpdateCrt })
                        setUpdateCrt(false)
                    },
                    (error) => {
                        console.log(error);
                    }
                );
        }

        if (updateEvPm == true) {
            var ev1 = { period: valueEvpm.interval, events: valueEvpm.eventList };
            Get_EvPM(ev1)
                .then(
                    (result) => {
                        toFixedValue(result.yaxis);
                        lineChartOptionsTotalSpent.xaxis.categories = result.xaxis;
                        var interval = valueEvpm.interval;
                        setValueEvmp({ data: result.yaxis, options: lineChartOptionsTotalSpent, interval: interval, update: setUpdateEvPM, eventList: valueEvpm.eventList })
                        setUpdateEvPM(false)
                    },
                    (error) => {
                        console.log(error);
                    }
                );
        }

    }
        , [updateCtr, updateEvPm])


    const toFixedValue = (yaxis) => {
        yaxis.forEach((diagram) => {
            diagram.data = diagram.data.map((data) => { return data.toFixed(3) });
        });
    }

    return (
        <SimpleGrid
            mb='20px'
            columns={{ sm: 1, md: 1 }}
            spacing={{ base: "20px", xl: "20px" }}>
            <DiagramPageContext.Provider
                value={{ value: valueCtr }}>
                <div>
                    <CtrSpent />
                </div>
            </DiagramPageContext.Provider>
            <DiagramPageContext.Provider value={{ value: valueEvpm }}>
                <div>
                    <EvpmSpent />
                </div>
            </DiagramPageContext.Provider>
        </SimpleGrid>
    );
}


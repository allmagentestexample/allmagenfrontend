// Chakra imports
import { Box } from "@chakra-ui/react";

import TablePage from "./TablePage"
import React from "react";

export default function Settings() {
  return (
    <Box pt={{ base: "130px", md: "80px", xl: "80px" }}>
      <TablePage />
    </Box>
  );
}

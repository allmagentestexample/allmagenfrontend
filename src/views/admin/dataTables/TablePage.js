import { SimpleGrid } from "@chakra-ui/react";
import ColumnsTable from "views/admin/dataTables/components/ColumnsTable";

import React, { useState, useEffect } from "react";
import { TableContext } from "contexts/TableContext";
import { GetTable } from "modem/statistic";

export default function Settings(props) {
  const { ...rest } = props;


  const [mm_dma, setMm_dma] = useState({
    data: [{
      name: "",
      impressions: "",
      ctr: "",
      evpm: {}
    }],
    param: "mm_dma",
    evpm_ev: "registration",
    update_func: "",
    setEventList: ""
  })

  const [site_id, setSite_id] = useState({
    data: [{
      name: "",
      impressions: "",
      ctr: "",
      evpm: {}
    }],
    param: "site_id",
    evpm_ev: "registration",
    update_func: "",
    setEventList: ""
  })

  const [downloadMM_dma, setDownloadMm_dma] = useState(false);
  const [downloadSite_id, setDownloadSite_id] = useState(false);

  useEffect(() => {
    if (!downloadMM_dma) {
      setDownloadMm_dma(true);
      setDownloadSite_id(true);

      var ev1 = { parameter: "mm_dma" };
      GetTable(ev1)
        .then(
          (result) => {
            var param = mm_dma;
            param.data = result.table;
            mm_dma.setEventList(result.list_event);
            setMm_dma(param)
            if (typeof mm_dma.update_func == "function")
              mm_dma.update_func(!mm_dma.update);
            setDownloadMm_dma(false);
          },
          (error) => {
            console.log(error);
            setDownloadMm_dma(false);
          }
        )
        .then(() => {
          if (!downloadSite_id) {

            var ev = { parameter: "site_id" };
            GetTable(ev)
              .then(
                (result) => {
                  var param = site_id;
                  param.data = result.table;
                  setSite_id(param)
                  site_id.setEventList(result.list_event);
                  if (typeof site_id.update_func == "function")
                    site_id.update_func(!site_id.update);
                  setDownloadSite_id(false);
                },
                (error) => {
                  console.log(error);
                  setDownloadSite_id(false);
                }
              );
          }
        }
        )
    }
  }, [])

  return (
    <SimpleGrid
      mb='20px'
      columns={{ sm: 1, md: 2 }}
      spacing={{ base: "20px", xl: "20px" }}>
      <TableContext.Provider
        value={{ value: mm_dma, setValue: setMm_dma, download: downloadMM_dma }}>
        <ColumnsTable />
      </TableContext.Provider>
      <TableContext.Provider
        value={{ value: site_id, setValue: setSite_id, download: downloadSite_id }}>
        <ColumnsTable />
      </TableContext.Provider>
    </SimpleGrid>
  );
}
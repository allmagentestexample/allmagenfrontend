import * as React from 'react';
import { styled } from '@mui/system';
import EvpmMenu from "./EvpmMenu"
import {
  TablePagination,
  tablePaginationClasses as classes,
} from '@mui/base/TablePagination';
import { TableContext } from 'contexts/TableContext';
import Card from "components/card/Card";
import {
  Flex,
  Table,
  Tfoot,
  Tbody,
  Td,
  Text,
  Th,
  Thead,
  Tr,
  useColorModeValue,
} from "@chakra-ui/react";



export default function TableCustomized() {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  // Avoid a layout jump when reaching the last page with empty rows.
  const textColor = useColorModeValue("secondaryGray.900", "white");

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };


  const value = React.useContext(TableContext);
  const [event, setEvent] = React.useState(value.value.evpm_ev)
  const [update, setUpdate] = React.useState(true)

  React.useEffect(() => {
    var v1 = value.value;
    v1.setEvent = setEvent
    v1.update_func = setUpdate;
    v1.update = update;
    value.setValue(v1);
    prepareData();
  }, [event, update])


  const [rows, setRows] = React.useState([]);

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - value.value.data.length) : 0;

  const prepareData = () => {
    var arr = value.value.data.map((value) => {
      var name = value.name;
      var impressions = value.impressions;
      var ctr = value.ctr;
      var evpm = "";
      if (value.evpm.hasOwnProperty(event)){
        evpm = value.evpm[event];
      }
      else {
        evpm = 0;
      }
      return { name, impressions, ctr, evpm }
    })
    setRows(arr);
  }

  const fixedParam = (value) => {
    if (typeof value == "number") {
      return value.toFixed(3);
    }
    return value;
  }


  return (
    <Card
      direction='column'
      w='100%'
      px='0px'
      overflowX={{ sm: "scroll", lg: "hidden" }}
      minH='80'>
        {value.download }
      {value.download &&
        <Flex align='center' w='100%' px='15px' py='10px'>
          <Text
            me='auto'
            color={textColor}
            fontSize='x3'
            fontWeight='700'
            lineHeight='100%'>
            Data is loading
          </Text>
        </Flex>
      }
      <Table variant='simple' color='gray.500' mb='24px'>
        <Thead>
          <Tr>
            <Th>
              <Flex
                justify='space-between'
                align='center'
                fontSize={{ sm: "10px", lg: "12px" }}
                color='gray.400'>
                {value.value.param}
              </Flex>
            </Th>
            <Th>
              <Flex
                justify='space-between'
                align='center'
                fontSize={{ sm: "10px", lg: "12px" }}
                color='gray.400'>
                Impressions
              </Flex>
            </Th>
            <Th>
              <Flex
                justify='space-between'
                align='center'
                fontSize={{ sm: "10px", lg: "12px" }}
                color='gray.400'>
                CTR
              </Flex>
            </Th>
            <Th>
              <Flex
                align='center'
                fontSize={{ sm: "10px", lg: "12px" }}
                color='gray.400'>
                EvPM_{event}
                <EvpmMenu />
              </Flex></Th>
          </Tr>
        </Thead>
        <Tbody >
          {
            (rowsPerPage > 0
              ? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              : rows
            ).map((row) => (
              <Tr key={row.name}>
                <Td>
                  <Text color={textColor} fontSize='sm' fontWeight='700'>
                    {row.name}
                  </Text>
                </Td>
                <Td>
                  <Text color={textColor} fontSize='sm' fontWeight='700'>
                    {row.impressions}
                  </Text>
                </Td>
                <Td style={{ width: 120 }} align="right">
                  <Flex align='center'>
                    <Text color={textColor} fontSize='sm' fontWeight='700'>
                      {fixedParam(row.ctr)}
                    </Text>
                  </Flex>
                </Td>
                <Td style={{ width: 120 }} align="right">
                  <Flex align='center'>
                    <Text color={textColor} fontSize='sm' fontWeight='700' align='center'>
                      {fixedParam(row.evpm)}
                    </Text>
                  </Flex>
                </Td>
              </Tr>
            ))}

          {emptyRows > 0 && (
            <tr style={{ height: 34 * emptyRows }}>
              <td colSpan={3} aria-hidden />
            </tr>
          )}
        </Tbody>
        <Tfoot>
          <Tr>
            <CustomTablePagination
              rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
              colSpan={3}
              count={rows.length}
              rowsPerPage={rowsPerPage}
              page={page}
              slotProps={{
                select: {
                  'aria-label': 'Отображать на странице',
                },
                actions: {
                  showFirstButton: true,
                  showLastButton: true,
                },
              }}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
            />
          </Tr>
        </Tfoot>
      </Table>
    </Card>


  );
}


const CustomTablePagination = styled(TablePagination)(
  ({ theme }) => `
  & .${classes.spacer} {
    display: none;
  }

  & .${classes.toolbar}  {
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    gap: 10px;

    @media (min-width: 768px) {
      flex-direction: row;
      align-items: center;
    }
  }

  & .${classes.selectLabel} {
    margin: 0;
  }

  & .${classes.select}{
    padding: 2px;
    border-radius: 50px;
    background-color: transparent;

    &:hover {
    }

    &:focus {
    }
  }

  & .${classes.displayedRows} {
    margin: 0;

    @media (min-width: 768px) {
      margin-left: auto;
    }
  }

  & .${classes.actions} {
    padding: 2px;
    border-radius: 50px;
    text-align: center;
  }

  & .${classes.actions} > button {
    margin: 0 8px;
    border: transparent;
    border-radius: 2px;
    background-color: transparent;

    &:hover {
    }

    &:focus {
    }
  }
  `,
);
import config from "config/config";

const serverUri = config.serverProtocol + "://" + config.serverHost + ":" + config.serverPort;

export const GetStatisticCount = async () => {

  var url = serverUri + "/api/get_count/";
  console.log(url)
  const response = await fetch(url);
  return await response.json();
};

export const GetEventList = async () => {
  var url = serverUri + "/api/get_event_list/";
  const response = await fetch(url);
  return await response.json();
};

export const GetTable = async (ev1) => {
  var url = serverUri + "/api/get_table/";
  const response = await fetch(url, {
    headers: {
      'Content-Type': 'application/json;charset=utf-8'
    },
    method: 'POST',
    body: JSON.stringify(ev1)
  })
  return await response.json();
}

export const Get_Ctr = async (ev1) => {
  var url = serverUri + "/api/get_ctr/";
  const responce = await fetch(url, {
    headers: {
      'Content-Type': 'application/json;charset=utf-8'
    },
    method: 'POST',
    body: JSON.stringify(ev1)
  });
  return await responce.json();
}

export const Get_EvPM = async (ev1) => {
  var url = serverUri + "/api/get_evpm/";
  const responce = await fetch(url, {
    headers: {
      'Content-Type': 'application/json;charset=utf-8'
    },
    method: 'POST',
    body: JSON.stringify(ev1)
  });
  return await responce.json();
}


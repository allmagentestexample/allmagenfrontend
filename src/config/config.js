export default {
    // IP_SERVER_STRING - IP адрес сервера BackEnd
    appTitle: "TestExample",
    serverHost: "localhost",
    serverProtocol: "http",
    // IP_SERVER_NUMBER - порт сервера BackEnd
    // serverPort: window.location.port //For production build
    serverPort: 8448 //For debug
};
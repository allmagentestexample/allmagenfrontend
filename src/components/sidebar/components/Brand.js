import React from "react";

// Chakra imports
import { Flex, useColorModeValue, Text } from "@chakra-ui/react";

// Custom components
import { HSeparator } from "components/separator/Separator";

export function SidebarBrand() {
  const textColor = useColorModeValue("secondaryGray.900", "white");
  //   Chakra color mode
  let logoColor = useColorModeValue("navy.700", "white");

  return (
    <Flex align='center' direction='column'  justify='space-between'>
      <Flex minH='110px' justify='center' direction='column'>
      <Text color={textColor}
        fontSize='30px'
        fontWeight='800'
        lineHeight='100%'
        align='center'>
        AllmaGen
      </Text>
      <Text
        align='center'
        fontSize='18px'
        color='gray.400'
        
        >
        Test example
      </Text>
      </Flex>
      <HSeparator mb='20px' />
    </Flex>
  );
}

export default SidebarBrand;
